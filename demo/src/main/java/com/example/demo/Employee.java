package com.example.demo;

import java.util.ArrayList;
import java.util.List;

public class Employee {
    private String name;
    private List<Address> address;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Address> getAddress() {
        if (address == null) {
            this.address = new ArrayList<>();
        }
        return this.address;
    }

    public void setAddress(List<Address> address) {
        this.address = address;
    }
}
