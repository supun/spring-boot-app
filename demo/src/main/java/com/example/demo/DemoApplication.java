package com.example.demo;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.*;

@Controller
@SpringBootApplication
@EnableAutoConfiguration(exclude = {
        org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class
})
public class DemoApplication {

    @Value("${welcome.message:test}")
    private String message = "Hello World";

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @RequestMapping("/")
    public String welcome(Map<String, Object> model) {
        model.put("message", this.message);
        model.put("link", UUID.randomUUID());
        return "welcome";
    }

    @RequestMapping("/map")
    public String getMap(Map<String, Object> model) {
        model.put("lng", 90);
        model.put("lat", 10);
        Coordinate coordinate = new Coordinate();
        coordinate.setLatitude(10);
        coordinate.setLongitude(5);
        model.put("center", coordinate);
        return "map";
    }

    @RequestMapping(value = "/formData", method = RequestMethod.GET)
    public String formData(Map<String, Object> model) {
        Employee employee = new Employee();
        Address address = new Address();
        address.setDescription("TEST");
        address.setEmployee(employee);
        employee.getAddress().add(address);
        model.put("employee", employee);
        List<String> products = new ArrayList<>();
        products.add("TEST1");
        products.add("TEST2");
        products.add("TEST3");
        model.put("products", products);
        return "formData";

    }

    @RequestMapping(value = "/employeeData", method = RequestMethod.POST)
    public void employeeData(@Valid Employee employeeData, BindingResult errors) {
        System.out.println(employeeData.getName());

    }

    @RequestMapping(value = "/image")
    public String employeeData(Model model) throws IOException {
        model.addAllAttributes(new ObjectMapper().readValue("{ \"dy_t1\": \"This is the text 2\" , \"dy_Lorem\":\"This is the lorem text\"}", HashMap.class));
        return "image";

    }

    @GetMapping("/weather")
    public String weatherForm(Model model) {
        model.addAttribute("weather", new WeatherServiceImpl());
        return "weather";
    }

    @PostMapping("/weather")
    public String weatherSubmit(WeatherServiceImpl weather, Model model) {
        model.addAttribute("weather", weather);
        return "result";
    }

    @RequestMapping("/login")
    public String login(Model model) {
        return "login";
    }

    @RequestMapping("/role")
    public String role(Model model) {
        return "role";
    }

    @RequestMapping("/selectForm")
    public String option(Model model) {
        List<Employee> employees = new ArrayList<>();
        Employee employee = new Employee();
        employee.setName("TEST1");
        employees.add(employee);
        model.addAttribute("employees", employees);
        VisitorForm visitorForm = new VisitorForm();
        model.addAttribute("visitorForm", visitorForm);
        return "option";
    }

}
