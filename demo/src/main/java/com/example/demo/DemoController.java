package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class DemoController {
    @Autowired
    private ISpyGirl spicy;


    @RequestMapping("/spicy")
    public String index() {
        return spicy.iSaySomething();
    }


    @RequestMapping(value = "/saveDistance/{distance}", method = RequestMethod.POST)
    public String saveDistance(@PathVariable String distance) {
        System.out.println(distance);
        return distance;
    }
}
