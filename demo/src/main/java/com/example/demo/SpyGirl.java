package com.example.demo;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class SpyGirl implements ISpyGirl {

    @Override
    public String iSaySomething() {
        return "spicy vagyok";
    }
}
