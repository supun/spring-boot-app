package com.example.demo;

import java.util.List;

public class VisitorForm {

    String id;
    String name;
    String address;
    String mobileNumber;
    String email;
    String purposeOfvisit;
    List<Employee> employees;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPurposeOfvisit() {
        return purposeOfvisit;
    }

    public void setPurposeOfvisit(String purposeOfvisit) {
        this.purposeOfvisit = purposeOfvisit;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }
}

